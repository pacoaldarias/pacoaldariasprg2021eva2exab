/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 12:40:14
 */
public class Alumno extends Persona {

   private String NIA;

   Alumno(int id_, String nombre_, String nia_) {
      this.setId(id_);
      this.setNombre(nombre_);
      NIA = nia_;
   }

   /**
    * @return the NIA
    */
   public String getNIA() {
      return NIA;
   }

   /**
    * @param NIA the NIA to set
    */
   public void setNIA(String NIA) {
      this.NIA = NIA;
   }

   @Override
   public String toSTring() {
      return this.getId() + " " + this.getNombre() + " " + this.getNIA();
   }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 12:50:43
 */
public class Fichero implements Almacen {

   private String falumno = "alumnos.txt";

   @Override
   public void grabar(Alumno a) {

      try {
         File f = new File(falumno);
         //FileWriter fw = new FileWriter(f, true);  // Para añadir
         FileWriter fw = new FileWriter(f);
         fw.write(a.getId() + ";" + a.getNombre() + ";" + a.getNIA() + "\n");
         System.out.println(a);
         fw.close();
      } catch (Exception ex) {
         ex.printStackTrace();
      }

   }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 13:12:24
 */
public class Miexcepcion extends Exception {

   private String texto;

   Miexcepcion(String t) {
      texto = t;
   }

   public String toString() {
      return texto;
   }

}

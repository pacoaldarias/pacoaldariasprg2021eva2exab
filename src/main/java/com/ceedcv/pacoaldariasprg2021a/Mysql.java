/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Mysql {

   private static Connection conn = null;

   public Mysql() {
      conectar();
   }

   /**
    * @param args the command line arguments
    */
   public static void conectar() {

      System.out.println("Conectando a la base de datos...");
      try {
         Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
         String url = "jdbc:mysql://172.18.0.2:3306/myDb?serverTimezone=UTC";
         conn = DriverManager.getConnection(url, "user", "test");
         System.out.println("OK!");
      } catch (SQLException e) {
         e.printStackTrace();
      } catch (ClassNotFoundException ex) {
         ex.printStackTrace();
      } catch (InstantiationException ex) {
         Logger.getLogger(Mysql.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IllegalAccessException ex) {
         Logger.getLogger(Mysql.class.getName()).log(Level.SEVERE, null, ex);
      }

   }

   public static boolean existeNIE(Alumno a) {
      boolean existe = true;

      try {
         String sql = "SELECT * FROM alumnos where nia='" + a.getNIA() + "';";
         //SELECT * FROM alumnos where nia='a12'
         //Statement st = conn.createStatement();
         Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
         ResultSet rs = st.executeQuery(sql);

         if (!rs.first()) {
            existe = false;
         }
         st.close();
         conn.close();

      } catch (SQLException ex) {
         ex.printStackTrace();
         return existe;
      }
      return existe;

   }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 12:35:19
 *
 */
public class Pregunta2 {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      // TODO code application logic here

      Alumno a1 = new Alumno(1, "Felipe", "A2");
      Alumno a2 = new Alumno(2, "Paco", "B2");
      Almacen alm = new Fichero();
      alm.grabar(a1);
      alm.grabar(a2);
      System.out.println(a1.toSTring());
      System.out.println(a2.toSTring());

   }

}
